import 'dart:async';
import 'dart:typed_data';

import 'package:image/image.dart';
import 'package:zxing_lib/aztec.dart';
import 'package:zxing_lib/common.dart';
import 'package:zxing_lib/zxing.dart';

typedef AztecImageDecoder = FutureOr<Uint8List?> Function(Image image);

/// The default Aztec decoder using [AztecReader].
///
/// It is considered pretty unreliably about the binary encoding. You should
/// rather specify as custom one.
Uint8List? defaultAztecReader(Image image) {
  final squareBytes = image.buffer.asUint8List();

  final pixels = Uint8List(image.width * image.height);
  for (int i = 0; i < pixels.length; i++) {
    pixels[i] = _getLuminanceSourcePixel(squareBytes, i * 4);
  }

  final imageSource = RGBLuminanceSource(
    image.width,
    image.height,
    squareBytes,
  );

  final bitmap = BinaryBitmap(HybridBinarizer(imageSource));

  final reader = AztecReader();

  final result = reader.decode(
    bitmap,
    DecodeHint(tryHarder: true, alsoInverted: true),
  );

  final bytes = result.rawBytes;
  if (bytes == null) return null;
  return Uint8List.fromList(bytes);
}

int _getLuminanceSourcePixel(List<int> byte, int index) {
  if (byte.length <= index + 3) {
    return 0xff;
  }
  final r = byte[index] & 0xff; // red
  final g2 = (byte[index + 1] << 1) & 0x1fe; // 2 * green
  final b = byte[index + 2]; // blue
  // Calculate green-favouring average cheaply
  return ((r + g2 + b) ~/ 4);
}
