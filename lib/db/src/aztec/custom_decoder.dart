import 'package:railway_subscriptions/db/src/aztec/default_decoder.dart';

AztecImageDecoder? _customAztecDecoder;

AztecImageDecoder? get customAztecDecoder => _customAztecDecoder;

setCustomAztecDecoder(AztecImageDecoder decoder) {
  if (_customAztecDecoder != null) return;
  _customAztecDecoder = decoder;
}
