import 'dart:typed_data';

import 'package:barcode/barcode.dart';
import 'package:intl/locale.dart';
import 'package:pkpass/pkpass.dart';

import 'api_response.dart';

extension PkPassTicketResponse on TicketResponse {}

class DbPassFile implements PassFile {
  final TicketResponse ticketResponse;

  static Future<DbPassFile> decode(TicketResponse ticketResponse) async {
    final metadata =
        await DbPassMetadata.decode(ticketResponse: ticketResponse);
    return DbPassFile._(ticketResponse, metadata);
  }

  const DbPassFile._(this.ticketResponse, this.metadata);

  @override
  final DbPassMetadata metadata;

  @override
  Uint8List? getBackground({Locale? locale, int scale = 1}) => null;

  @override
  Uint8List? getFooter({Locale? locale, int scale = 1}) => null;

  @override
  Uint8List? getIcon({Locale? locale, int scale = 1}) =>
      ticketResponse.tickets.firstOrNull?.payload.header.logo;

  @override
  Map<String, String>? getLocalizations(Locale? locale) => null;

  @override
  Uint8List? getLogo({Locale? locale, int scale = 1}) =>
      ticketResponse.tickets.firstOrNull?.payload.header.logo;

  @override
  Uint8List? getStrip({Locale? locale, int scale = 1}) => null;

  @override
  Uint8List? getThumbnail({Locale? locale, int scale = 1}) => null;
}

class DbPassMetadata extends PassMetadata {
  final TicketResponse ticketResponse;

  static Future<DbPassMetadata> decode({
    required TicketResponse ticketResponse,
  }) async {
    final barcodes = await Future.wait(
      ticketResponse.tickets.map(
        (ticket) async {
          final barcode = await ticket.payload.additionalElements.parseTicket();
          if (barcode == null) return null;
          return PassBarcode(
            format: BarcodeType.Aztec,
            barcodeData: barcode,
            altText: ticketResponse.ticketCover.productName,
          );
        },
      ),
    );

    return DbPassMetadata._(
      ticketResponse: ticketResponse,
      barcodes: barcodes.whereType<PassBarcode>().toList(),
    );
  }

  DbPassMetadata._({
    required this.ticketResponse,
    required super.barcodes,
  }) : super(
          formatVersion: 1,
          description: ticketResponse.ticketCover.productName,
          organizationName: ticketResponse
                  .tickets.firstOrNull?.payload.trafficAssociation.fullName ??
              '',
          teamIdentifier: ticketResponse.deviceToken,
          serialNumber: ticketResponse.ticketCover.orderNumber,
          passTypeIdentifier: ticketResponse.ticketCover.productName,
          logoText: ticketResponse
              .tickets.firstOrNull?.payload.trafficAssociation.fullName,
          boardingPass: DbBoardingPass(
            ticketResponse.tickets.first,
            ticketResponse.ticketCover,
          ),
          relevantDate:
              ticketResponse.tickets.firstOrNull?.payload.firstDayOfValidity,
          expirationDate:
              ticketResponse.tickets.firstOrNull?.payload.expiryDate,
        );
}

class DbBoardingPass extends PassStructureDictionary {
  final Ticket ticket;
  final TicketCover cover;

  DbBoardingPass(this.ticket, this.cover)
      : super(
          transitType: TransitType.train,
          headerFields: [
            DictionaryField(
              key: 'TRAVELER',
              label: 'Name',
              value: StringDictionaryValue(ticket.payload.customer.toString()),
              textAlignment: PassTextAlign.left,
            ),
            DictionaryField(
              key: 'TICKET',
              label: 'Ticket',
              value: StringDictionaryValue(
                '${ticket.payload.validityAreaDescription}, ${cover.travelClass}. class',
              ),
              textAlignment: PassTextAlign.right,
            ),
            ticket.payload.firstDayOfValidity
                ?.toPassDictionaryField('FROM', 'Valid from'),
            ticket.payload.expiryDate
                ?.toPassDictionaryField('TO', 'Valid until'),
          ].whereType<DictionaryField>().toList(),
          primaryFields: [
            DictionaryField(
              key: 'ISSUER',
              label: 'Issued by',
              value: StringDictionaryValue(
                  '${ticket.payload.trafficAssociation.fullName} (${ticket.payload.trafficAssociation.tariffCode})'),
            ),
            DictionaryField(
              key: 'ORDER_ID',
              label: 'Subscription number',
              value: StringDictionaryValue(cover.orderNumber),
            ),
          ],
        );
}

extension on DateTime {
  toPassDictionaryField(String key, String label) => DictionaryField(
        key: key,
        label: label,
        value: DateTimeDictionaryValue(this),
      );
}
