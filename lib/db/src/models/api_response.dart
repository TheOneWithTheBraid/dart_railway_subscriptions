import 'dart:convert';
import 'dart:typed_data';

import 'package:image/image.dart';

import 'package:railway_subscriptions/db/src/aztec/custom_decoder.dart';
import 'package:railway_subscriptions/db/src/aztec/default_decoder.dart';

class TicketResponse {
  /// the device token used to refresh the ticket
  final String deviceToken;

  /// contained ticket models
  final List<Ticket> tickets;

  /// style only : an accent color to be used with the ticket
  final String associationColor;

  /// information at a glace - the virtual ticket coverlet
  final TicketCover ticketCover;

  /// some verification metadata required for refresh
  final VerificationImage verificationImage;

  final TicketTexts ticketTexts;

  const TicketResponse({
    required this.deviceToken,
    required this.tickets,
    required this.associationColor,
    required this.ticketCover,
    required this.verificationImage,
    required this.ticketTexts,
  });

  factory TicketResponse.fromJson(Map<String, dynamic> json) => TicketResponse(
      deviceToken: json['deviceToken'],
      tickets:
          (json['tickets'] as List).map((i) => Ticket.fromJson(i)).toList(),
      associationColor: json['verbundColor'],
      ticketCover: TicketCover.fromJson(json['ticketHuelle']),
      verificationImage: VerificationImage.fromJson(json['lichtbild']),
      ticketTexts: TicketTexts.fromJson(json['ticketTexte']));

  Map<String, dynamic> toJson() => {
        'deviceToken': deviceToken,
        'tickets': tickets.map((i) => i.toJson()).toList(),
        'verbundColor': associationColor,
        'ticketHuelle': ticketCover.toJson(),
        'lichtbild': verificationImage.toJson(),
        'ticketTexte': ticketTexts.toJson(),
      };
}

/// the required metadata for a ticket refresh request
class RefreshData {
  /// equal to [TicketResponse.deviceToken]
  final String deviceToken;

  /// equal to [VerificationImage.signature]
  final String verificationImageSignature;

  /// equal to [TicketTexts.hash]
  final String textHash;

  /// equal to [TicketPayload.token]
  final List<String> ticketTokens;

  /// seemed to be [false] so far
  final bool hasVerificationImage;

  const RefreshData({
    required this.deviceToken,
    required this.verificationImageSignature,
    required this.textHash,
    required this.ticketTokens,
    required this.hasVerificationImage,
  });

  /// creates a new refresh data set from the given [response]
  ///
  /// Just a tiny helper
  factory RefreshData.fromTicketResponse(TicketResponse response) {
    final deviceToken = response.deviceToken;
    final verificationImageSignature = response.verificationImage.signature;
    final textHash = response.ticketTexts.hash;
    final ticketTokens =
        response.tickets.map((ticket) => ticket.payload.token).toList();
    // no clue what it's supposed to mean
    final hasVerificationImage = false;

    return RefreshData(
      deviceToken: deviceToken,
      verificationImageSignature: verificationImageSignature,
      textHash: textHash,
      ticketTokens: ticketTokens,
      hasVerificationImage: hasVerificationImage,
    );
  }

  Map<String, Object?> toJson() {
    return {
      "aboTicketCheckRequestList": [
        {
          "deviceToken": deviceToken,
          "hasLichtbild": hasVerificationImage,
          "lichtbildSignature": verificationImageSignature,
          "textHash": textHash,
          "ticketTokenList": ticketTokens,
        }
      ]
    };
  }
}

/// the ticket summary at a glace
class TicketCover {
  /// the order id of the subscription
  final String orderNumber;

  /// the travel class
  final String travelClass;

  /// the human readable name of the subscription
  final String productName;

  const TicketCover({
    required this.orderNumber,
    required this.travelClass,
    required this.productName,
  });

  factory TicketCover.fromJson(Map<String, dynamic> json) => TicketCover(
        orderNumber: json['auftragsnummer'],
        travelClass: json['klasse'],
        productName: json['produktname'],
      );

  Map<String, dynamic> toJson() => {
        'auftragsnummer': orderNumber,
        'klasse': travelClass,
        'produktname': productName,
      };
}

/// wrapper around the base64 encoded [TicketPayload]
class Ticket {
  /// the base64 encoded header
  final String header;

  /// the decoded payload
  final TicketPayload payload;

  /// the base64 encoded signature
  final String signature;

  const Ticket({
    required this.header,
    required this.payload,
    required this.signature,
  });

  factory Ticket.fromJson(Map<String, dynamic> json) => Ticket(
        header: json['header'],
        payload: TicketPayload.fromBase64(json['payload']),
        signature: json['signature'],
      );

  Map<String, dynamic> toJson() => {
        'header': header,
        'payload': payload.toJson(),
        'signature': signature,
      };
}

/// no clue what it's used for - but it's relevant for subscription refresh
class VerificationImage {
  /// the base64 encoded header
  final String header;

  /// contains a no further documented base64 String saying "status: new" in JSON
  final String payload;

  /// the base64 encoded signature
  final String signature;

  const VerificationImage({
    required this.header,
    required this.payload,
    required this.signature,
  });

  factory VerificationImage.fromJson(Map<String, dynamic> json) =>
      VerificationImage(
        header: json['header'],
        payload: json['payload'],
        signature: json['signature'],
      );

  Map<String, dynamic> toJson() => {
        'header': header,
        'payload': payload,
        'signature': signature,
      };
}

/// some text information contained in the ticket
class TicketTexts {
  /// this hash seems to be relevant for refresh
  final String hash;

  const TicketTexts({
    required this.hash,
  });

  factory TicketTexts.fromJson(Map<String, dynamic> json) => TicketTexts(
        hash: json['hash'],
      );

  Map<String, dynamic> toJson() => {
        'hash': hash,
      };
}

/// the actual payload representing a ticket
class TicketPayload {
  /// text describing the area of validity of the ticket
  final String validityAreaDescription;

  /// the last day of validity of the ticket
  final DateTime? expiryDate;
  final AdditionalElements additionalElements;

  /// information about the traffic association or carrier
  final TrafficAssociation trafficAssociation;

  /// time the subscription started
  final DateTime? subscribedSince;

  final String id;
  final String? barcode;
  final Customer customer;
  final DateTime? firstDayOfValidity;
  final String token;
  final TicketLayout layout;
  final String ticketColor;
  final Header header;

  const TicketPayload({
    required this.validityAreaDescription,
    required this.expiryDate,
    required this.additionalElements,
    required this.trafficAssociation,
    required this.subscribedSince,
    required this.id,
    required this.barcode,
    required this.customer,
    required this.firstDayOfValidity,
    required this.token,
    required this.layout,
    required this.ticketColor,
    required this.header,
  });

  factory TicketPayload.fromBase64(String base64) => TicketPayload.fromJson(
        jsonDecode(
          utf8.decode(
            base64Decode(Base64Codec().normalize(base64)),
          ),
        ),
      );

  factory TicketPayload.fromJson(Map<String, dynamic> json) => TicketPayload(
        validityAreaDescription: json['geltungsbereich'],
        expiryDate: DateTimeDMYExtension.fromDMY(json['letzterGeltungstag']),
        additionalElements:
            AdditionalElements.fromJson(json['additionalElements']),
        trafficAssociation: TrafficAssociation.fromJson(json['verbundInfo']),
        subscribedSince: DateTimeDMYExtension.fromDMY(json['aboBestehtSeit']),
        id: json['id'],
        barcode: json['barcode'] is String ? json['barcode'] : null,
        customer: Customer.fromJson(json['kunde']),
        firstDayOfValidity:
            DateTimeDMYExtension.fromDMY(json['ersterGeltungstag']),
        token: json['token'],
        layout: TicketLayoutExtension.fromJson(json['layout']),
        ticketColor: json['ticketColor'],
        header: Header.fromJson(json['header']),
      );

  Map<String, dynamic> toJson() => {
        'geltungsbereich': validityAreaDescription,
        'letzterGeltungstag': expiryDate,
        'additionalElements': additionalElements.toJson(),
        'verbundInfo': trafficAssociation.toJson(),
        'aboBestehtSeit': subscribedSince,
        'id': id,
        'barcode': barcode,
        'kunde': customer.toJson(),
        'ersterGeltungstag': firstDayOfValidity,
        'token': token,
        'layout': layout.toJson(),
        'ticketColor': ticketColor,
        'header': header.toJson(),
      };

  static Uint8List? _imageHelper(String? baseString) {
    if (baseString == null) return null;
    final header = RegExp(r'^data:(\w+/\w+;)?base64,');
    baseString = baseString.replaceFirst(header, '');
    // this is not my fault *grumble*
    baseString = baseString.replaceAll(r'\', '');
    return base64.decode(base64.normalize(baseString));
  }
}

/// might contain a base64 encoded PNG of a railway ticket
///
/// Usually use [parseTicket] to decode to a String for use in an Aztec
class AdditionalElements {
  /// the raw bytes of the PNG
  final Uint8List? ticketPicture;

  const AdditionalElements({
    required this.ticketPicture,
  });

  factory AdditionalElements.fromJson(Map<String, dynamic> json) =>
      AdditionalElements(
        ticketPicture: TicketPayload._imageHelper(json['ticketPicture']),
      );

  Map<String, dynamic> toJson() {
    final ticketPicture = this.ticketPicture;
    return {
      'ticketPicture': ticketPicture == null
          ? null
          : 'data:image/png;base64,${base64.encode(ticketPicture)}',
    };
  }

  Image _cropCenterAztec(Image image) {
    final dimension = image.width * 3 + 64;

    Image square = copyCrop(
      image,
      width: image.width,
      height: image.width + 128,
      x: 0,
      y: 0,
    );

    square = copyResize(
      square,
      width: dimension,
      height: dimension,
      maintainAspect: true,
      backgroundColor: ColorUint8.rgb(255, 255, 255),
    );
    return square;
  }

  /// parses a ticket
  ///
  /// - decodes the byte list into an image
  /// - optimizes the size to focus on the Aztec (best guess)
  /// - binary encodes into high/low brightness
  /// - applies the Aztec reader
  /// - returns the String extracted from the Aztec reader
  Future<Uint8List?> parseTicket() async {
    final ticketPicture = this.ticketPicture;
    if (ticketPicture == null) return null;

    final decoded = decodeImage(ticketPicture);
    if (decoded == null) return null;

    final square = _cropCenterAztec(decoded);

    final decoder = customAztecDecoder ?? defaultAztecReader;

    return await decoder.call(square);
  }
}

/// represents the traffic association the ticket is valid in
class TrafficAssociation {
  /// abbreviation of the association name
  final String shortName;

  /// fully qualified name of the carrier
  final String fullName;

  /// carrier code (not UIC !)
  final int tariffCode;

  const TrafficAssociation({
    required this.shortName,
    required this.fullName,
    required this.tariffCode,
  });

  factory TrafficAssociation.fromJson(Map<String, dynamic> json) =>
      TrafficAssociation(
        shortName: json['kurzbezeichnung'],
        fullName: json['langbezeichnung'],
        tariffCode: json['tarifgebercode'],
      );

  Map<String, dynamic> toJson() => {
        'kurzbezeichnung': shortName,
        'langbezeichnung': fullName,
        'tarifgebercode': tariffCode,
      };
}

/// the customer the ticket is valid for
class Customer {
  /// the date of birth
  final DateTime? birthday;

  /// first name
  final String firstName;

  /// surname
  final String familyName;

  const Customer({
    required this.birthday,
    required this.firstName,
    required this.familyName,
  });

  factory Customer.fromJson(Map<String, dynamic> json) => Customer(
        birthday: DateTimeDMYExtension.fromDMY(json['geburtsdatum']),
        firstName: json['vorname'],
        familyName: json['nachname'],
      );

  Map<String, dynamic> toJson() => {
        'geburtsdatum': birthday?.toDMY(),
        'vorname': firstName,
        'nachname': familyName,
      };

  @override
  String toString() => '$firstName $familyName';
}

/// header image of the ticket
class Header {
  /// the date time when the ticket was last refreshed
  final DateTime dateTime;

  /// the PNG content of the logo
  final Uint8List? logo;

  /// the name of the ticket in German with the validity period - better do not use
  final String title;

  const Header({
    required this.dateTime,
    required this.logo,
    required this.title,
  });

  factory Header.fromJson(Map<String, dynamic> json) => Header(
        dateTime: DateTime.parse(json['dateTime']),
        logo: TicketPayload._imageHelper(json['logo']),
        title: json['title'],
      );

  Map<String, dynamic> toJson() {
    final logo = this.logo;
    return {
      'dateTime': dateTime.toIso8601String(),
      'logo':
          logo == null ? null : 'data:image/png;base64,${base64.encode(logo)}',
      'title': title,
    };
  }
}

/// the layout for the ticket
///
/// Currently only [pictureLayout] supported
enum TicketLayout {
  pictureLayout,
}

extension TicketLayoutExtension on TicketLayout {
  static const Map<TicketLayout, String> _names = {
    TicketLayout.pictureLayout: 'PICTURE_LAYOUT'
  };

  static TicketLayout fromJson(String name) =>
      _names.entries.singleWhere((element) => element.value == name).key;

  String toJson() {
    return _names[this] ?? toString();
  }
}

extension DateTimeDMYExtension on DateTime {
  static DateTime? fromDMY(String? dmy) {
    if (dmy == null) return null;
    final regex = RegExp(r'^(\d{2})\.(\d{2})\.(\d{4})$');
    final match = regex.firstMatch(dmy);
    if (match == null) return null;

    final dayString = match.group(3);
    if (dayString == null) return null;
    final day = int.parse(dayString);

    final monthString = match.group(3);
    if (monthString == null) return null;
    final month = int.parse(monthString);

    final yearString = match.group(3);
    if (yearString == null) return null;
    final year = int.parse(yearString);

    return DateTime(year, month, day);
  }

  String toDMY() =>
      ('${_makeTwoCharacters(day)}.${_makeTwoCharacters(month)}.$year');

  String _makeTwoCharacters(int number) {
    final expanded = '0$number';
    return expanded.substring(expanded.length - 2, 2);
  }
}
