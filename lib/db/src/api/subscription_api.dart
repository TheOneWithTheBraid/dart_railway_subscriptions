import 'dart:convert';
import 'dart:math';

import 'package:http/http.dart';

import '../../db.dart';

class SubscriptionApi {
  /// provide a custom [Client] for all HTTP requests
  final Client client;

  /// a 16 character hex String persistent across the sessions
  final String deviceId;

  static final Uri prodApi = Uri.https('dig-aboprod.noncd.db.de');

  static const _headers = {
    'x-api-version': '5',
    'accept': 'application/json',
    'content-type': 'application/json',
    'x-user-agent': 'com.deutschebahn.abo.navigatorV2.modul',
    'x-app-id': 'de.hafas.android.db',
  };

  SubscriptionApi(Client? client, this.deviceId) : client = client ?? Client();

  /// generates a new String matching a [deviceId]
  static String generateDeviceId() {
    final random = Random();
    final bytes = List.generate(16, (index) => random.nextInt(16));
    return bytes.map((e) => e.toRadixString(16)).join();
  }

  Map<String, String> _makeHeaders() => {
        ..._headers,
        'x-unique-id': deviceId,
      };

  /// searches for a subscription
  ///
  /// The provided [name] is the last name of the passenger
  /// The provided [subscriptionNumber] is the order number issued by the carrier
  Future<TicketResponse> searchSubscription({
    required String subscriptionNumber,
    required String name,
  }) async {
    final response = await client.get(
      prodApi.resolveUri(
        Uri(
          path: '/aboticket',
          queryParameters: {
            // subscription number
            'abonummer': subscriptionNumber,
            // last name
            'nachname': name,
          },
        ),
      ),
      headers: _makeHeaders(),
    );
    switch (response.statusCode) {
      case 404:
        throw SubscriptionNotFoundError();
      case 401:
        throw SubscriptionAlreadyClaimedError();
      case 200:
        return TicketResponse.fromJson(jsonDecode(response.body));
      default:
        throw ApiHttpError(response.statusCode);
    }
  }

  /// checks for updates of a subscription
  ///
  /// if the response contains new [TicketResponse.tickets], it likely updated. Otherwise ignore.
  Future<TicketResponse> refreshMultiple(RefreshData refreshData) async {
    final response = await client.post(
      prodApi.resolveUri(
        Uri(path: '/aboticket/refreshmultiple'),
      ),
      headers: _makeHeaders(),
      body: jsonEncode(refreshData.toJson()),
    );
    switch (response.statusCode) {
      case 404:
        throw SubscriptionNotFoundError();
      case 401:
        throw SubscriptionAlreadyClaimedError();
      case 200:
        return TicketResponse.fromJson(jsonDecode(response.body));
      default:
        throw ApiHttpError(response.statusCode);
    }
  }

  /// removes a ticket (identified by the [deviceToken]) from your local device
  ///
  /// Does not relate to canceling a subscription
  Future<void> logout(String deviceToken) async {
    final response = await client.post(
      prodApi.resolveUri(
        Uri(path: '/aboticket/logout'),
      ),
      headers: _makeHeaders(),
      body: jsonEncode(
        {
          "deviceToken": deviceToken,
        },
      ),
    );
    switch (response.statusCode) {
      case 200:
        return;
      default:
        throw ApiHttpError(response.statusCode);
    }
  }
}
