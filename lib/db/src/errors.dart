/// abstract error class
abstract class ApiError extends Error {
  /// error message
  final String message;

  ApiError(this.message);

  @override
  String toString() => '$runtimeType: $message';
}

/// 404 - railway edition
class SubscriptionNotFoundError extends ApiError {
  SubscriptionNotFoundError() : super('404 - Subscription not found');
}

/// 401 - railway edition
class SubscriptionAlreadyClaimedError extends ApiError {
  SubscriptionAlreadyClaimedError()
      : super('401 - Subscription already claimed by another device.');
}

/// some error we do not expect
class ApiHttpError extends ApiError {
  final int statusCode;

  ApiHttpError(this.statusCode) : super('Http error: $statusCode');
}
