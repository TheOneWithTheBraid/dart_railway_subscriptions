# railway_subscriptions

Access proprietary railway subscription APIs.

## Supported

- Deutsche Bahn:
    - Picture Layout
    - *no regular tickets, only tickets showing up as pixilated image in your DB app*

## A note on Aztec decoding

Some tickets only contain the ticket barcode as image data - often an Aztec. This package provides basic parsing for it
using [package:zxing_lib](https://pub.dev/packages/zxing_lib). It turned out to be pretty unreliable. It's hence
recommended to rather provide a custom Aztec parser - the zxing-cpp implementation via FFI (Dart bindings) turned out to
be the only one functional yet. All others, especially the original zxing Java implementation, turned out to decode
invalid binary data from the ticket images.

Use to following snippet to provide a custom Aztec decoder :

```dart
import 'dart:typed_data';

import 'package:image/image.dart';
import 'package:railway_subscriptions/railway_subscriptions.dart';

void main() {
  // a callback of type [AztecImageDecoder] => Uint8List? Function(Image image);
  AztecImageDecoder myCustomDecoder=(Image image) {
    /// ...
  };

  setCustomAztecDecoder(myCustomDecoder);
}
```

## Additional information

Available under the terms and conditions of EUPL-1.2. Copy left ! Use in open source applications only !
